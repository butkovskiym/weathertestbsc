import Foundation

class Weather {
    
    var scale: Scale
    var cities = [City]()
    
    init(scale: Scale) {
        self.scale = scale
    }
    
    func changeScale() {
        scale = scale == .celcius ? .farenheit : .celcius
    }
    
    var scaleButtonTitle: NSMutableAttributedString {
        let string = "°C/°F"
        let usedUnit = scale == .celcius ? "°C" : "°F"
        let unusedUnit = scale == .celcius ? "°F" : "°C"
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.setColor(color: .darkGray, forText: "/")
        attributedString.setColor(color: .darkGray, forText: usedUnit)
        attributedString.setColor(color: .lightGray, forText: unusedUnit)
        
        return attributedString
    }
    
}
