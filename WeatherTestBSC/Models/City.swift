import Foundation

struct City {
    
    var name: String
    let temp: Double
    let description: String
    
    init(name: String, temp: Double, description: String) {
        self.name = name
        self.temp = temp
        self.description = description
    }
    
    func getTempStringForUI(in scale: Scale) -> String {
        switch scale {
        case .celcius:
            let roundedValue = Int(temp.rounded(.toNearestOrEven))
            return "\(roundedValue)°"
        case .farenheit:
            let tempInFahrenheit = temp * 1.8 + 32
            let roundedValue = Int(tempInFahrenheit.rounded(.toNearestOrEven))
            return "\(roundedValue)°"
        }
    }
    
}

extension City: Equatable {
    
    static func == (lhs: City, rhs: City) -> Bool {
        return lhs.name == rhs.name
    }
    
}
