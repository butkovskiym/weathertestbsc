import Foundation

enum Scale: String {
    case celcius
    case farenheit
}
