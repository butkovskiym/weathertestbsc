import Foundation

class DownloadManager {
    
    static let current = DownloadManager()
    private init(){}
    
    func getCurrentWeather(for cityName: String, completion: @escaping (City?, String?) -> Void) {
        let apiCaller = APICaller.current
        let geoNamesRequest = GeoNamesRequest(cityName: cityName)
        apiCaller.getData(with: geoNamesRequest) { (object: GeoNamesObject?, error) in
            guard error == nil else {
                completion(nil, error)
                return
            }
            guard let geoname = object?.geonames.first else {
                let error = "[\(String(describing: self))] \(#function) Geoname is nil"
                completion(nil, error)
                return
            }
            let openWeatherMapRequest = OpenWeatherMapRequest(toponymName: geoname.toponymName)
            apiCaller.getData(with: openWeatherMapRequest, completion: { (object: OpenWeatherMapObject?, error) in
                guard error == nil else {
                    completion(nil, error)
                    return
                }
                guard let main = object?.main else {
                    let error = "[\(String(describing: self))] \(#function) Main is nil"
                    completion(nil, error)
                    return
                }
                guard let weather = object?.weather.first else {
                    let error = "[\(String(describing: self))] \(#function) Weather is nil"
                    completion(nil, error)
                    return
                }
                let city = City(name: geoname.name, temp: main.temp, description: weather.description.firstCapitalized)
                completion(city, nil)
            })
        }
    }
    
}
