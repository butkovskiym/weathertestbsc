import Foundation

protocol Request {
    
    func configure() -> URLRequest?
    
}
