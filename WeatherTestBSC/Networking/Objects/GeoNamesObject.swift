import Foundation

struct GeoNamesObject: Codable {
    
    struct GeoName: Codable {
        let toponymName: String
        let name: String
    }
    
    let totalResultsCount: Int
    let geonames: [GeoName]
    
}
