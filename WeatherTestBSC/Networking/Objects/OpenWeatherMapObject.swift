import Foundation

struct OpenWeatherMapObject: Codable {
    
    struct Weather: Codable {
        let description: String
    }
    
    struct Main: Codable {
        let temp: Double
    }
    
    let weather: [Weather]
    let main: Main
    
}
