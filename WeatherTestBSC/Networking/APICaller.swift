import Foundation

class APICaller {
    
    static let current = APICaller()
    private init(){}
    
    func getData<T: Codable>(with request: Request, completion: @escaping (T?, String?) -> Void) {
        guard let request = request.configure() else {
            let error = "[\(String(describing: self))] \(#function) Error configure request"
            completion(nil, error)
            return
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                let error = "[\(String(describing: self))] \(#function) Error: \(error!.localizedDescription)"
                DispatchQueue.main.async { completion(nil, error) }
                return
            }
            guard let response = response as? HTTPURLResponse else {
                let error = "[\(String(describing: self))] \(#function) Invalid response"
                DispatchQueue.main.async { completion(nil, error) }
                return
            }
            guard response.statusCode == 200 else {
                let error = "[\(String(describing: self))] \(#function) HTTP error: \(response.statusCode)"
                DispatchQueue.main.async { completion(nil, error) }
                return
            }
            guard let data = data else {
                let error = "[\(String(describing: self))] \(#function) Error get data"
                DispatchQueue.main.async { completion(nil, error) }
                return
            }
            guard let object = try? JSONDecoder().decode(T.self, from: data) else {
                let error = "[\(String(describing: self))] \(#function) Error decode object from data"
                DispatchQueue.main.async { completion(nil, error) }
                return
            }
            DispatchQueue.main.async { completion(object, nil) }
        }
        task.resume()
    }
    
}
