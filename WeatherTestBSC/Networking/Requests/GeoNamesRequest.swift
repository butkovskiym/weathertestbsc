import Foundation

class GeoNamesRequest: Request {
    
    var cityName: String
    
    init(cityName: String) {
        self.cityName = cityName
    }
    
    func configure() -> URLRequest? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "api.geonames.org"
        urlComponents.path = "/searchJSON"
        let name_equals = URLQueryItem(name: "name_equals", value: cityName)
        let lang = URLQueryItem(name: "lang", value: "ru")
        let searchLang = URLQueryItem(name: "searchLang", value: "ru")
        let maxRows = URLQueryItem(name: "maxRows", value: "1")
        let username = URLQueryItem(name: "username", value: "butkovskiym")
        urlComponents.queryItems = [name_equals, lang, searchLang, maxRows, username]
        
        guard let url = urlComponents.url else {
            print("[\(String(describing: self))] \(#function) Invalide URL")
            return nil
        }
        print(url)
        let request = URLRequest(url: url)
        
        return request
    }
    
}
