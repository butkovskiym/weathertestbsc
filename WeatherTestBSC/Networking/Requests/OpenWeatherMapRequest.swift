import Foundation

class OpenWeatherMapRequest: Request {

    var toponymName: String
    
    init(toponymName: String) {
        self.toponymName = toponymName
    }
    
    func configure() -> URLRequest? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.openweathermap.org"
        urlComponents.path = "/data/2.5/weather"
        let q = URLQueryItem(name: "q", value: toponymName)
        let lang = URLQueryItem(name: "lang", value: "ru")
        let units = URLQueryItem(name: "units", value: "metric")
        let appID = URLQueryItem(name: "APPID", value: "de4ee2b48100054b79ccdce2ff96a5f5")
        urlComponents.queryItems = [q, lang, units, appID]
        
        guard let url = urlComponents.url else {
            print("[\(String(describing: self))] \(#function) Invalide URL")
            return nil
        }
        print(url)
        let request = URLRequest(url: url)
        
        return request
    }
    
}
