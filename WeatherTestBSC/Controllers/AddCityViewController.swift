import UIKit

class AddCityViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var nameTextField: UITextField!
    
    // MARK: - Properties
    
    var weather: Weather!
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.addBottomBorderWithColor(color: .lightGray, width: 1)
    }
    
    // MARK: - Helper methods
    
    func configureAlert(with message: String) -> UIAlertController? {
        let alertController = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ок", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        return alertController
    }
    
    // MARK: - IBActions
    
    @IBAction func okButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        let downloadManager = DownloadManager.current
        let cityName = nameTextField.text!
        downloadManager.getCurrentWeather(for: cityName) { [weak self] (city, errorDescription) in
            if errorDescription != nil {
                print(errorDescription!)
                let message = "Невозможно добавить город с таким наименованием"
                guard let alertController = self?.configureAlert(with: message) else { return }
                self?.present(alertController, animated: true)
            } else {
                guard let city = city else { return }
                guard let selfCities = self?.weather.cities else { return }
                if selfCities.contains(city) {
                    let message = "Город с таким наименованием уже существует"
                    guard let alertController = self?.configureAlert(with: message) else { return }
                    self?.present(alertController, animated: true)
                } else {
                    self?.weather.cities.append(city)
                    self?.performSegue(withIdentifier: "CityAddedUnwindSegue", sender: nil)
                }
            }
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
}
