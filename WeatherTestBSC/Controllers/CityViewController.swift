import UIKit

class CityViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    // MARK: - Properties
    
    var city: City?
    var scale: Scale!
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = city?.name ?? "ДОБАВЬТЕ ГОРОД"
        descriptionLabel.text = city?.description ?? nil
        tempLabel.text = city?.getTempStringForUI(in: scale) ?? nil
    }

}

