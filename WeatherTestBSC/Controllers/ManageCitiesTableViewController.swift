import UIKit

class ManageCitiesTableViewController: UITableViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var scaleButton: UIButton!
    
    // MARK: - Properties
    
    var weather: Weather!
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scaleButton.setAttributedTitle(weather.scaleButtonTitle, for: .normal)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weather.cities.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        let city = weather.cities[indexPath.row]
        cell.textLabel?.text = city.name
        cell.detailTextLabel?.text = city.getTempStringForUI(in: weather.scale)

        return cell
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            weather.cities.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func scaleButtonTapped(_ sender: UIButton) {
        weather.changeScale()
        scaleButton.setAttributedTitle(weather.scaleButtonTitle, for: .normal)
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
    }
    
    @IBAction func cityAddedUnwindSegue(segue: UIStoryboardSegue) {
        tableView.reloadData()
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "AddCitySegue":
            guard let dvc = segue.destination as? AddCityViewController else { return }
            dvc.weather = weather
        default:
            return
        }
    }

}
