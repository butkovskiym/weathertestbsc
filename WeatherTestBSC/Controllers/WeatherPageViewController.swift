import UIKit

class WeatherPageViewController: UIPageViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    // MARK: - Properties
    
    let weather = Weather(scale: .celcius)
    var viewControllersForPages: [CityViewController]!

    // MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        view.backgroundColor = .lightGray
        
        viewControllersForPages = configureViewControllersForPages()
        setViewControllers([viewControllersForPages[0]], direction: .forward, animated: true, completion: nil)
        pageControl.currentPage = 0
        pageControl.numberOfPages = viewControllersForPages.count
    }
    
    // MARK: - Helper methods
    
    func configureViewControllersForPages() -> [CityViewController] {
        var result = [CityViewController]()
        if weather.cities.count > 0 {
            for city in weather.cities {
                guard let viewController = storyboard?.instantiateViewController(withIdentifier: "CityViewController") as? CityViewController else { break }
                viewController.city = city
                viewController.scale = weather.scale
                result.append(viewController)
            }
        } else {
            guard let viewController = storyboard?.instantiateViewController(withIdentifier: "CityViewController") as? CityViewController else { return [] }
            viewController.city = nil
            viewController.scale = weather.scale
            result.append(viewController)
        }
        
        return result
    }
    
    // MARK: - IBActions
    
    @IBAction func manageCitiesDone(segue: UIStoryboardSegue) {
        viewControllersForPages = configureViewControllersForPages()
        DispatchQueue.main.async() { [weak self] in
            self?.dataSource = nil
            self?.dataSource = self
        }
        setViewControllers([viewControllersForPages[0]], direction: .forward, animated: true, completion: nil)
        pageControl.currentPage = 0
        pageControl.numberOfPages = viewControllersForPages.count
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ManageCitiesSegue":
            guard let dvc = segue.destination as? ManageCitiesTableViewController else { return }
            dvc.weather = weather
        default:
            return
        }
    }

}

// MARK: - Page view controller data source

extension WeatherPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? CityViewController else { return nil }
        guard let index = viewControllersForPages.index(of: viewController) else { return nil }
        
        return index > 0 ? viewControllersForPages[index - 1] : nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? CityViewController else { return nil }
        guard let index = viewControllersForPages.index(of: viewController) else { return nil }
        
        return index < weather.cities.count - 1 ? viewControllersForPages[index + 1] : nil
    }
    
}

// MARK: - Page view controller delegate

extension WeatherPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else { return }
        guard let currentViewController = pageViewController.viewControllers?.first as? CityViewController else { return }
        guard let currentPage = viewControllersForPages.index(of: currentViewController) else { return }
        pageControl.currentPage = currentPage
    }
    
}
